country_decisions = {

	vrorenmarch_nation = {
		major = yes
		potential = {
			culture_group = escanni
			NOT = { culture_is_half_orcish = yes }
			normal_or_historical_nations = yes
			was_never_end_game_tag_trigger = yes
			NOT = { has_country_flag = formed_vrorenmarch_flag }
			NOT = { exists = Z45 } #Vrorenmarch doesn't exist
			
			NOT = { has_country_flag = feudal_escann_kingdom }	#prevents countries like Blademarches from forming

			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		provinces_to_highlight = {
			OR = {
				province_id = 740	#Bal Vroren
				province_id = 735
				province_id = 749
				province_id = 737
				province_id = 738
				province_id = 739
				province_id = 742
			}
			OR = {
				NOT = { owned_by = ROOT }
				NOT = { is_core = ROOT }
			}
		}
		allow = {
			adm_tech = 7
			government_rank = 1
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 10
			owns_core_province = 740	#Bal Vroren
			owns_core_province = 735
			owns_core_province = 749
			owns_core_province = 737
			owns_core_province = 738
			owns_core_province = 739
			owns_core_province = 742
		}
		effect = {
			set_country_flag = feudal_escann_kingdom
			change_tag = Z45
			remove_non_electors_emperors_from_empire_effect = yes
			if = {
				limit = {
					NOT = { government_rank = 2 }
				}
				set_government_rank = 2
			}
			if = {
				limit = {
					has_reform = adventurer_reform
				}
				set_country_flag = adventurer_derived_government
			}
			hidden_effect = {
				every_owned_province = {
					limit = {
						has_owner_culture = yes
					}
					change_culture = white_reachman
				}
			}
			change_primary_culture = white_reachman
			
			#Claims
			vrorenmarch_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = Z45
			}
			gulletpeak_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = Z45
			}
			cedesck_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = Z45
			}
			vrorenwall_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = Z45
			}
			sondaar_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = Z45
			}
			wudhal_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = Z45
			}
			east_chillsbay_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = Z45
			}
			drowned_giant_isles_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = Z45
			}
			add_prestige = 50
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			set_country_flag = formed_vrorenmarch_flag
			clr_country_flag = 	knightly_order_adventurer
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
}