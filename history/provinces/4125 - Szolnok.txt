


culture = black_orc
religion = old_dookan
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 2
base_manpower = 1
native_size = 12
native_ferocity = 4
native_hostileness = 3
add_permanent_province_modifier = {
	name = flooded_province
	duration = -1
}