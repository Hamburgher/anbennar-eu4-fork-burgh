#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 180 62 138 }

revolutionary_colors = { 180 62 138 }

historical_idea_groups = {
	aristocracy_ideas
	offensive_ideas
	expansion_ideas
	quantity_ideas	
	administrative_ideas	
	trade_ideas
	quality_ideas
	innovativeness_ideas
}

ship_names = {
	Dazar Zoi Talzar Tzai Nare Tzidoil Tozgon Khohol Zakheel Bokh Barikh Erek Dazar Dzor Dozen Zer Tolgod Tengen Golkhin Tseig
	Serei Sarts Neisar Tolg Usrek Akal Nuuk Dazkhui Dazjil Galsui Toltar Tsor Orkhon Kamdhil Enkhiin Kavkhil Orghon Tarzoi Dolin Tzhum
	Akuuunar Unarsrek Shum Akanhuluu Ruk Lutar Zur Bazur Zurka Nzuura Kozuuri Khill T�khei Usrek Dalkhi Gak Agaak Naar Abosrek Onakh Oncha
	Itgavar Arkh Kesh Yaru Sirem Aluun Chor Sagat Ang�i Evdel Ult Alkho Qharuul H��kud Noosh Mudh Eniikh Estav Kuldaakin Izen Qazmoq Khinrch
	Cergu Qarsht R�ton Olg Kov Oghen Ianakal Khuga Tareg Dazjal Araga Saklyn
}

army_names = {
	"Bridge Guard"
}

fleet_names = {
	"Passguard Fleet"
}