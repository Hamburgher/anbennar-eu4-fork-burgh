#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 218 154 24 }

revolutionary_colors = { 218 154 24 }

historical_idea_groups = {
	aristocracy_ideas
	offensive_ideas
	expansion_ideas
	quantity_ideas	
	administrative_ideas	
	trade_ideas
	quality_ideas
	innovativeness_ideas
}

historical_units = {
	western_medieval_infantry
	chevauchee
	western_men_at_arms
	swiss_landsknechten
	dutch_maurician
	french_caracolle
	anglofrench_line
	french_dragoon
	french_bluecoat
	french_cuirassier
	french_impulse
	mixed_order_infantry
	open_order_cavalry
	napoleonic_square
	napoleonic_lancers
}

ship_names = {
	Barruqh Oryonqur Kagaydon Qaylaqur Laroq Qashlaroq Tayon Tushlaq Soyil Keyayil Sayonyil Yukar Doyen Yik Yiklon Tayeri
	Tarmik Tisk Bahkikkel Oyolkel Kelqur Qerma Qeyish Payik Jarik Zeng Qodiyi Zabatkeya Yalgliq Engaz Yerqari Yikil Kelikil Jaryik
	Zabtozh Zabtay Yukarkeya Qustran Kel Qur Say yavik Qayik Sayin Qayin Koyik Qoron Qurin Narin Mashoq Baxin Ayab Baxinuya Umhozu
	Oqar Ernarin Eryerin Baqur Qism Sabiqeya Savozq Nakish Nayza Orpayik Qizim Movish kayig Sarik Sakrim Qirshim Juyarang Urkeya Narkeya Kurnal
	Borzkal Kelnazim Baquyeri Bocenki Qayakeya Tolra Osuya Qoqov Mizaq Sovkogar Parshqar Baquri Don'pachi Orqug Pavormaq Janxar To'vachi Eskha Qaysh Zoxim
}

army_names = {
	"Heartland Guard"
}

fleet_names = {
	"Fleet of the Glade"
}